<div class="slider slider--big">
  <div class="slider-inner js--slider slider--dots">
    <div class="slider-item" style="background-image:url('assets/images/slider/anantara-kihavah-first-class-holiday.jpg')"></div>
    <div class="slider-item" style="background-image:url('assets/images/slider/anantara-kihavah-maldives-island-resort.jpg')"></div>
    <div class="slider-item" style="background-image:url('assets/images/slider/anantara-kihavah-maldives-overwater-spa.jpg')"></div>
    <div class="slider-item" style="background-image:url('assets/images/slider/anantara-kihavah-maldives-villa-walkway.jpg')"></div>
  </div>

  <div class="slider-container px--4 px--md--0">
    <div class="container--xl pb--4 pt--8 py--md--8">
      <p class="text--meta mt--3 mb--3 mt--md--5 mb--md--5">Destinationer / Det indiske ocean</p>
      <h3 class="color--main font--italic mb--4">Rejser til 
        <br>
        Maldiverne
      </h3>
      <img class="mb--5 mb--md--8 image--contain" src="assets/images/slider-graphic.png" alt="">
      <div class="js--slider-actions slider-actions"></div>
    </div>
  </div>
</div>
<!-- Slider big end. -->

<!-- Page nav -->
<div class="toggle toggle--visibility toggle--sm">
  <input type="checkbox" id="navPage" class="toggle-checkbox">
  <label class="nav-pageToggle toggle-label" for="navPage">Menu <i class="icon--arrowDownDark ml--1"></i></label>
  <div class="nav-page toggle-container">
    <div class="container--xl">
      <nav class="nav nav--page">
        <div class="nav-list">
          <div class="nav-item"><a href="#section--1" class="nav-link">Om Maldiverne</a></div>
          <div class="nav-item"><a href="#section--2" class="nav-link">Atoller</a></div>
          <div class="nav-item"><a href="#section--3" class="nav-link">Aktiviteter</a></div>
          <div class="nav-item"><a href="#section--4" class="nav-link">Klima Og Vejr</a></div>
          <div class="nav-item"><a href="#section--5" class="nav-link">Fakta Om Tanzania</a></div>
          <div class="nav-item"><a href="#section--6" class="nav-link">Hotller Pa Maldiverne</a></div>
        </div>
      </nav>
      <a href="#" class="button button--main button--big button--navPage">Se Alle Rejser Til Maldiverne</a>
    </div>
  </div>
</div>
<!-- Page nav end -->

<section class="section--wide py--4 py--md--8 bg-variant-2--1" id="section--1">
  <div class="container--md">
    <p class="section-title mb--3 mb--md--7 text--center text--md--left">Om Maldiverne</p>

    <div class="text-widget grid">
      <div class="col--12 col--md--7 mb--3 mb--md--0">

        <!-- Slider small -->
        <div class="slider slider--small mb--4">
          <div class="slider-inner js--slider">
            <div class="slider-item">
              <a href="assets/images/slider-image--small.jpg" data-lightbox="slider--2">
                <img class="image--contain" src="assets/images/slider-image--small.jpg">
              </a>
            </div>
            <div class="slider-item">
              <a href="assets/images/slider-image--small.jpg" data-lightbox="slider--2">
                <img class="image--contain" src="assets/images/slider-image--small.jpg">
              </a>
            </div>
            <div class="slider-item">
              <a href="assets/images/slider-image--small.jpg" data-lightbox="slider--2">
                <img class="image--contain" src="assets/images/slider-image--small.jpg">
              </a>
            </div>
          </div>
          <div class="slider-expand">Store fotos</div>
          <div class="slider-container">
            <div class="js--slider-actions slider-actions">
            </div>
          </div>
        </div>
        <!-- Slider small end. -->

        <img class="image--contain image--right mb--6" src="assets/images/text-map-2.png" alt="">

        <p class="p--offset--left p--icon--plane">Rejsen til Maldiverne er typisk med afrejse fra Kastrup, Billund eller Hamborg med mellemlanding og flyskift i en europæisk storby, Qatar eller Dubai afhængig af flyselskabet. Flyvetiden er sammenlagt fra 11 timer afhængig af ruten. Den samlede rejsetid afhænger derfor af, hvor lang tid man har til at skifte fly undervejs. Til Maldiverne benytter vi oftest flyselskaberne Emirates, Qatar Airways, Etihad, Condor, Swiss/Edelweiss og Austrian.</p>

        <p class="p--offset--left">Transport videre til hotellet er altid inkluderet i vores rejser ved rejser til Maldiverne med Blixen Tours har vi altid inkluderet transport fra lufthavn i Malé til hotellet tur/retur. Enten med båd eller vandflyver og til de sydligst liggende øer indenrigsfly og båd.</p>


      </div>
      <div class="col--12 col--md--5">
        <p class="text--leader text--size--reset">
          Når du nærmer dig Maldiverne fra luften, fornemmer du at være på vej til noget særligt. Så langt øjet rækker ses små koraløer som perler i det azurblå hav. Øerne åbenbarer sig som et tropeparadis med sol, vand og varme. Kridhvide strande med blidt svajende kokospalmer. Fugle i fantastiske farver glider henover det klareste hav, du kan forestille dig.
        </p>
        <p>
          Tag på en eksotisk rejse til Maldiverne. Fordriv ferie tiden med dykning og surfing eller sejl rundt mellem de godt 1.200 småøer, hvor nogle af verdens smukkeste og mest farverige koralrev og krystalklare laguner findes. Rejs på eventyr og oplev det fantastiske fugleliv og smag på mangoer, appelsiner og ananas.
          Bo i lækre bungalows langs strandkanten, eller nyd livet på de mest fantastiske hoteller med værelser, der står på pæle i vandet. Disse ”water-villas” er en helt unik og fantastisk oplevelse som kendetegner rejser til Maldiverne. Mange vælger at tage på en romantisk bryllupsrejse til dette ø-paradis, og vi forstår godt hvorfor.
        </p>
        <p>
          Maldiverne er et sandt paradis både over og under vandet. En betagende og farverig verden dukker frem så snart du kommer under havoverfladen. Elsker du derfor at dykke eller snorkle er Maldiverne det helt rigtige valg.
        </p>
        <p>
          Hold ferie på Maldiverne året rundt. Maldiverne byder jer nemlig varmt velkommen med sol og kridhvide bountystrande og temperaturer året rundt på omkring 30 grader.
          En rejse til Maldiverne byder på alt hvad du drømmer om, hvis du er til ultimativ luksus på en lille bounty-ø. Til trods for at øerne er små, er der et utroligt højt serviceniveau samt udbyd af aktiviteter. Maldiverne rejser kan derfor byde på langt mere end bare en ferie på en kridhvid sandstrand. Der kan være stor forskel på øerne på Maldiverne, så forhør dig derfor gerne hos os om, hvilken ø der passer bedst til jeres ønsker. 
        </p>
        <p>
          Når du rejser til Maldiverne med Blixen Tours er det muligt at flyve fra de større danske lufthavne - Kastrup og Billund - samt Hamborg. Der kan i visse perioder være en prismæssig fordel i at rejse til Maldiverne fra Tyskland. Skriv eller ring og hør nærmere om dine muligheder.
        </p>
        <a href="#" class="text--smaller"><strong><u><i class="font--serif">Fuld beskrivelse</i></u></strong></a>
      </div>
    </div>
    
  </div>
</section> <!-- section 1 -->
<section class="section--wide bg-variant-2--1" id="section--2">
  <div class="pt--4 pb--3 pt--md--8 pb--md--6 bg-lighten--1">
    <p class="text--center section-title mb--4 mb--md--7">populære safarirejser til tanzania</p>
    <div class="tour-list tour-list--popular tour-carousel carousel js--carousel mb--5">
      <?php for($i=0;$i<15;$i++){include('partials/tour-list-item.php');} ?>
    </div>
    <p class="text--center">
      <a href="#" class="button button--main button--big button--navPage">Se Alle Rejser Til Maldiverne</a>
    </p>
  </div>
</section> <!-- section 2 -->
<section class="section--wide py--4 py--md--8 bg-variant-4--1" id="section--3">
  <div class="container--md">
    <p class="section-title mb--3 mb--md--7 text--center text--md--left">
      områder og parker <br> tanzania
      <a href="" class="float--right font--italic text--underlined text--capitalize text--strong">Fuld beskrivelse</a>
    </p>
    <div class="accordion">
      <div class="accordion-header">
        <i class="icon--number mr--2">1</i>
        Sanctuary Chief´s Camp
      </div>
      <h3 class="accordion-header">
        <i class="icon--number mr--2">2</i>
        Sanctuary Chief´s Camp
      </h3>
      <div class="accordion-content">
        <div class="text-widget grid mb--6">
          <div class="col--12 col--md--6">
            
            <img class="image--contain mb--1" src="assets/images/accordion-image.jpg">
            <p class="text--smaller">
              Serengeti Nationalpark
            </p>

          </div>
          <div class="col--12 col--md--6">
            <p>Navnet Serengeti kommer fra ”siringet”, der på Masai-folkets sprog betyder “endeløse sletter”, og med rette. Serengeti er et vildtområde på 14.763 km², hvor godt to tredjedele er sletter. I den nordligere del er landskabet meget kuperet, og i den centrale del er der tæt akacievegetation. </p>
            <p>I Serengeti finder man en bestand af storvildt, som ikke findes mage til andre steder på jorden. Mod nord grænser parken op til Masai Mara-reservatet i Kenya. De to parker danner et økosystem, hvor omkring to millioner dyr er på konstant vandring (”migration” på engelsk) efter nye græsgange – størstedelen er gnuer, zebraer og gazeller. Rovdyrene følger flokken som en skygge, altid på jagt efter næste måltid. </p>
            <p>I Serengeti er alle vildtarter repræsenteret, og safarien byder altid på spændende oplevelser. Perioden fra omkring december til juni er de bedste måneder at opleve migration.</p>
          </div>
        </div>
      </div> <!-- accordion-content -->

      <h3 class="accordion-header">
        <i class="icon--number mr--2">3</i>
        Sanctuary Chief´s Camp
      </h3>
      <div class="accordion-content">
        <div class="text-widget grid mb--6">
          <div class="col--12 col--md--6">
            
            <img class="image--contain mb--1" src="assets/images/accordion-image.jpg">
            <p class="text--smaller">
              Serengeti Nationalpark
            </p>

          </div>
          <div class="col--12 col--md--6">
            <p>Navnet Serengeti kommer fra ”siringet”, der på Masai-folkets sprog betyder “endeløse sletter”, og med rette. Serengeti er et vildtområde på 14.763 km², hvor godt to tredjedele er sletter. I den nordligere del er landskabet meget kuperet, og i den centrale del er der tæt akacievegetation. </p>
            <p>I Serengeti finder man en bestand af storvildt, som ikke findes mage til andre steder på jorden. Mod nord grænser parken op til Masai Mara-reservatet i Kenya. De to parker danner et økosystem, hvor omkring to millioner dyr er på konstant vandring (”migration” på engelsk) efter nye græsgange – størstedelen er gnuer, zebraer og gazeller. Rovdyrene følger flokken som en skygge, altid på jagt efter næste måltid. </p>
            <p>I Serengeti er alle vildtarter repræsenteret, og safarien byder altid på spændende oplevelser. Perioden fra omkring december til juni er de bedste måneder at opleve migration.</p>
          </div>
        </div>
      </div> <!-- accordion-content -->

      <h3 class="accordion-header">
        <i class="icon--number mr--2">4</i>
        Sanctuary Chief´s Camp
      </h3>
      <div class="accordion-content">
        <div class="text-widget grid mb--6">
          <div class="col--12 col--md--6">
            
            <img class="image--contain mb--1" src="assets/images/accordion-image.jpg">
            <p class="text--smaller">
              Serengeti Nationalpark
            </p>

          </div>
          <div class="col--12 col--md--6">
            <p>Navnet Serengeti kommer fra ”siringet”, der på Masai-folkets sprog betyder “endeløse sletter”, og med rette. Serengeti er et vildtområde på 14.763 km², hvor godt to tredjedele er sletter. I den nordligere del er landskabet meget kuperet, og i den centrale del er der tæt akacievegetation. </p>
            <p>I Serengeti finder man en bestand af storvildt, som ikke findes mage til andre steder på jorden. Mod nord grænser parken op til Masai Mara-reservatet i Kenya. De to parker danner et økosystem, hvor omkring to millioner dyr er på konstant vandring (”migration” på engelsk) efter nye græsgange – størstedelen er gnuer, zebraer og gazeller. Rovdyrene følger flokken som en skygge, altid på jagt efter næste måltid. </p>
            <p>I Serengeti er alle vildtarter repræsenteret, og safarien byder altid på spændende oplevelser. Perioden fra omkring december til juni er de bedste måneder at opleve migration.</p>
          </div>
        </div>
      </div> <!-- accordion-content -->

      <h3 class="accordion-header">
        <i class="icon--number mr--2">5</i>
        Sanctuary Chief´s Camp
      </h3>
      <div class="accordion-content">
        <div class="text-widget grid mb--6">
          <div class="col--12 col--md--6">
            
            <img class="image--contain mb--1" src="assets/images/accordion-image.jpg">
            <p class="text--smaller">
              Serengeti Nationalpark
            </p>

          </div>
          <div class="col--12 col--md--6">
            <p>Navnet Serengeti kommer fra ”siringet”, der på Masai-folkets sprog betyder “endeløse sletter”, og med rette. Serengeti er et vildtområde på 14.763 km², hvor godt to tredjedele er sletter. I den nordligere del er landskabet meget kuperet, og i den centrale del er der tæt akacievegetation. </p>
            <p>I Serengeti finder man en bestand af storvildt, som ikke findes mage til andre steder på jorden. Mod nord grænser parken op til Masai Mara-reservatet i Kenya. De to parker danner et økosystem, hvor omkring to millioner dyr er på konstant vandring (”migration” på engelsk) efter nye græsgange – størstedelen er gnuer, zebraer og gazeller. Rovdyrene følger flokken som en skygge, altid på jagt efter næste måltid. </p>
            <p>I Serengeti er alle vildtarter repræsenteret, og safarien byder altid på spændende oplevelser. Perioden fra omkring december til juni er de bedste måneder at opleve migration.</p>
          </div>
        </div>
      </div> <!-- accordion-content -->

      <h3 class="accordion-header">
        <i class="icon--number mr--2">6</i>
        Sanctuary Chief´s Camp
      </h3>
      <div class="accordion-content">
        <div class="text-widget grid mb--6">
          <div class="col--12 col--md--6">
            
            <img class="image--contain mb--1" src="assets/images/accordion-image.jpg">
            <p class="text--smaller">
              Serengeti Nationalpark
            </p>

          </div>
          <div class="col--12 col--md--6">
            <p>Navnet Serengeti kommer fra ”siringet”, der på Masai-folkets sprog betyder “endeløse sletter”, og med rette. Serengeti er et vildtområde på 14.763 km², hvor godt to tredjedele er sletter. I den nordligere del er landskabet meget kuperet, og i den centrale del er der tæt akacievegetation. </p>
            <p>I Serengeti finder man en bestand af storvildt, som ikke findes mage til andre steder på jorden. Mod nord grænser parken op til Masai Mara-reservatet i Kenya. De to parker danner et økosystem, hvor omkring to millioner dyr er på konstant vandring (”migration” på engelsk) efter nye græsgange – størstedelen er gnuer, zebraer og gazeller. Rovdyrene følger flokken som en skygge, altid på jagt efter næste måltid. </p>
            <p>I Serengeti er alle vildtarter repræsenteret, og safarien byder altid på spændende oplevelser. Perioden fra omkring december til juni er de bedste måneder at opleve migration.</p>
          </div>
        </div>
      </div> <!-- accordion-content -->
      
    </div>
  </div>
</section> <!-- section 3 -->
<section class="section--wide bg-variant-4--1" id="section--4">
  <div class="container--fluid">
    <div class="grid">
      <div class="col--12 col--md--7 px--0">
       
        <div class="js--map map-widget w--100" id="map">
        </div>

      </div>
      <div class="col--12 col--md--5" style="background: #333333">
        
        <div class="map-legend px--0 px--md--2 px--xl--2 py--5">
          <p class="section-title mb--5">områder og parker <br> Botswana luksus safarirejse</p>
          <div class="list list--bordered list--2columns">
            
            <div class="list-title hidden">Områder og parker</div>
            <div class="list-item"><i class="icon--number mr--2">1</i>Sanctuary Chief´s Camp</div>
            <div class="list-item"><i class="icon--number mr--2">2</i>Sanctuary Chief´s Camp</div>
            <div class="list-item"><i class="icon--number mr--2">3</i>Sanctuary Chief´s Camp</div>
            <div class="list-item"><i class="icon--number mr--2">4</i>Sanctuary Chief´s Camp</div>
            <div class="list-item"><i class="icon--number mr--2">4</i>Sanctuary Chief´s Camp</div>
            <div class="list-item"><i class="icon--number mr--2">4</i>Sanctuary Chief´s Camp</div>
            <div class="list-item"><i class="icon--number mr--2">4</i>Sanctuary Chief´s Camp</div>
            <div class="list-item"><i class="icon--number mr--2">4</i>Sanctuary Chief´s Camp</div>
            <div class="list-item"><i class="icon--number mr--2">4</i>Sanctuary Chief´s Camp</div>
            <div class="list-item"><i class="icon--number mr--2">4</i>Sanctuary Chief´s Camp</div>
            <div class="list-item"><i class="icon--number mr--2">4</i>Sanctuary Chief´s Camp</div>
            <div class="list-item"><i class="icon--number mr--2">4</i>Sanctuary Chief´s Camp</div>
            <div class="list-item"><i class="icon--number mr--2">4</i>Sanctuary Chief´s Camp</div>
            <div class="list-item"><i class="icon--number mr--2">4</i>Sanctuary Chief´s Camp</div>
            <div class="list-item"><i class="icon--number mr--2">4</i>Sanctuary Chief´s Camp</div>
            <div class="list-item"><i class="icon--number mr--2">4</i>Sanctuary Chief´s Camp</div>
            <div class="list-item"><i class="icon--number mr--2">4</i>Sanctuary Chief´s Camp</div>
            <div class="list-item"><i class="icon--number mr--2">4</i>Sanctuary Chief´s Camp</div>

          </div>
        </div>

      </div>
    </div>
    
  </div>
</section> <!-- section 4 -->
<section class="section--wide py--4 py--md--8 bg-variant-4--1" id="section--5">
  <div class="container--md">
    <p class="section-title mb--3 mb--md--7 text--center text--md--left">Atoller <br> maldiverne</p>

    <div class="text-widget grid">
      <div class="col--12 col--md--7">

        <!-- Slider small -->
        <div class="slider slider--small mb--4">
          <div class="slider-inner js--slider">
            <div class="slider-item">
              <a href="assets/images/slider-image--small.jpg" data-lightbox="slider--2">
                <img class="image--contain" src="assets/images/slider-image--small.jpg">
              </a>
            </div>
            <div class="slider-item">
              <a href="assets/images/slider-image--small.jpg" data-lightbox="slider--2">
                <img class="image--contain" src="assets/images/slider-image--small.jpg">
              </a>
            </div>
            <div class="slider-item">
              <a href="assets/images/slider-image--small.jpg" data-lightbox="slider--2">
                <img class="image--contain" src="assets/images/slider-image--small.jpg">
              </a>
            </div>
          </div>
          <div class="slider-expand">Store fotos</div>
          <div class="slider-container">
            <div class="js--slider-actions slider-actions">
            </div>
          </div>
        </div>
        <!-- Slider small end. -->

      </div>
      <div class="col--12 col--md--5">
        <p class="text--leader text--size--reset">
          Maldiverne består af cirka 1.200 øer, der er samlet i 26 større klynger af øer - nærmere betegnet atoller - som er opstået omkring vulkaner og senere er sunket i havet. Kun cirka 200 af øerne er beboede.
        </p>
        <p>
          Øerne er tilsammen på størrelse med halvdelen af Bornholm. Den sydligste del af den 750 kilometer lange øgruppe krydser ækvator og højeste punkt er blot 1,5 meter over havets overflade. Mange atoller ses kun som turkise aftegninger under havoverfladen. En naturoplevelse i sig selv at betragte fra en vandflyver og en ekstra dimension til rejsen til Maldiverne.
        </p>
        <p>
          Hver atolgruppe på Maldiverne har et navn og sin egen administrative enhed, nærmest at sammenligne med amter.
        </p>


      </div>
    </div>
    
  </div>
</section> <!-- section 5 -->
<section class="section--wide py--4 py--md--8 bg-variant-2--1" id="section--6">
  <div class="container--md">
    <p class="section-title mb--3 mb--md--7 text--center text--md--left">Klima og vejr <br> <span>Maldiverne</span></p>

    <div class="text-widget grid mb--5 mb--md--8">
      <div class="col--12 col--md--6">

        <p class="text--leader">
          Vi bliver ofte spurgt om ’hvornår er det bedste tidspunkt at rejse til Maldiverne?’ Du kan besøge Maldiverne året rundt.
        </p>

      </div>
      <div class="col--12 col--md--6">
        <p class="">
          Maldiverne er en særdeles attraktiv rejsedestination året rundt. På grund af øgruppens beliggenhed uden for cyklonbæltet og tæt ved ækvator skinner solen de fleste dage, og dagtemperaturen ligger omkring 30 °C uanset årstiden. Nattemperaturen omkring 25-26 30 °C. Temperaturen varierer meget lidt, og varmen er behagelig - og så er havvandet altid lunt; omkring 28 grader året rundt.
        </p>
        <p>
          <a href="#" class="text--smaller"><strong><u><i class="font--serif">Fuld beskrivelse</i></u></strong></a>
        </p>
      </div>
    </div>

    <div class="mb--5 mb--md--8">
      <h5 class="text--uppercase">Lufttemperatur</h5>
      <div class="temperature-widget">
        <div class="temperature-month">
          <div class="temperature-monthTitle">Januar</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner" style="width: 80px;">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Februar</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner" style="width: 110px;">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Marts</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner" style="width: 115px;">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">April</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner" style="width: 50px;">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Maj</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Juni</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Juli</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">August</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">September</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Oktober</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">November</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">December</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
      </div>
    </div>

    <div class="mb--5 mb--md--8">
      <h5 class="text--uppercase">Vandtemperatur</h5>
      <div class="temperature-widget temperature-widget--half">
        <div class="temperature-month">
          <div class="temperature-monthTitle">Januar</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner" style="width: 80px;">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Februar</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner" style="width: 110px;">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Marts</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner" style="width: 115px;">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">April</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner" style="width: 50px;">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Maj</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Juni</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Juli</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">August</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">September</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Oktober</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">November</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">December</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
      </div>
    </div>

    <div>
      <h5 class="text--uppercase">Nedbør</h5>
      <div class="temperature-widget temperature-widget--striped">
        <div class="temperature-month">
          <div class="temperature-monthTitle">Januar</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner" style="width: 80px;">
            </div>
          </div>
          <div class="temperature-monthTemperature">92</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Februar</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner" style="width: 50px;">
            </div>
          </div>
          <div class="temperature-monthTemperature">22</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Marts</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner" style="width: 65px;">
            </div>
          </div>
          <div class="temperature-monthTemperature">58</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">April</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner" style="width: 85px;">
            </div>
          </div>
          <div class="temperature-monthTemperature">108</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Maj</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner" style="width: 115px;">
            </div>
          </div>
          <div class="temperature-monthTemperature">224</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Juni</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">162</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Juli</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">145</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">August</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">189</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">September</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">208</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Oktober</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">228</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">November</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">230</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">December</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">198</div>
        </div>
      </div>
    </div>
    
  </div>
</section> <!-- section 6 -->

<section class="section--wide py--4 py--md--8 bg-variant-2--1" id="section--">
  <div class="container--md">
    <p class="section-title mb--3 mb--md--7 text--center text--md--left">Atoller <br> maldiverne</p>

    <div class="text-widget grid">
      <div class="col--12 col--md--7">

        <!-- Slider small -->
        <div class="slider slider--small mb--4">
          <div class="slider-inner js--slider">
            <div class="slider-item">
              <a href="assets/images/slider-image--small.jpg" data-lightbox="slider--2">
                <img class="image--contain" src="assets/images/slider-image--small.jpg">
              </a>
            </div>
            <div class="slider-item">
              <a href="assets/images/slider-image--small.jpg" data-lightbox="slider--2">
                <img class="image--contain" src="assets/images/slider-image--small.jpg">
              </a>
            </div>
            <div class="slider-item">
              <a href="assets/images/slider-image--small.jpg" data-lightbox="slider--2">
                <img class="image--contain" src="assets/images/slider-image--small.jpg">
              </a>
            </div>
          </div>
          <div class="slider-expand">Store fotos</div>
          <div class="slider-container">
            <div class="js--slider-actions slider-actions">
            </div>
          </div>
        </div>
        <!-- Slider small end. -->

      </div>
      <div class="col--12 col--md--5">
        <p class="text--leader text--size--reset">
          Maldiverne består af cirka 1.200 øer, der er samlet i 26 større klynger af øer - nærmere betegnet atoller - som er opstået omkring vulkaner og senere er sunket i havet. Kun cirka 200 af øerne er beboede.
        </p>
        <p>
          Øerne er tilsammen på størrelse med halvdelen af Bornholm. Den sydligste del af den 750 kilometer lange øgruppe krydser ækvator og højeste punkt er blot 1,5 meter over havets overflade. Mange atoller ses kun som turkise aftegninger under havoverfladen. En naturoplevelse i sig selv at betragte fra en vandflyver og en ekstra dimension til rejsen til Maldiverne.
        </p>
        <p>
          Hver atolgruppe på Maldiverne har et navn og sin egen administrative enhed, nærmest at sammenligne med amter.
        </p>

        <div class="list list--bordered list--2columns">
          
          <div class="list-item">Sanctuary Chief´s Camp</div>
          <div class="list-item">Sanctuary Chief´s Camp</div>
          <div class="list-item">Sanctuary Chief´s Camp</div>
          <div class="list-item">Sanctuary Chief´s Camp</div>
          <div class="list-item">Sanctuary Chief´s Camp</div>
          <div class="list-item">Sanctuary Chief´s Camp</div>
          <div class="list-item">Sanctuary Chief´s Camp</div>

        </div>


      </div>
    </div>
    
  </div>
</section> <!-- section -->
<section class="section--wide search--section bg-variant-2--1" id="section--7">

  <div class="search-filters--mobile hidden--md--up">
    <div class="search-filterQuery">
      <i class="icon--search"></i>
      <input type="text" placeholder="What are you looking for?">
      <i class="js--search-filterTrigger icon--close"></i>
    </div>
    <div class="js--search-filterTrigger search-filterTrigger bg-darken--3">
      <span class="icon--sliders mr--3"></span>
      <span class="color--white">Filter results <small class="hidden--md--up">(182)</small></span>
      <span class="icon--arrowRightWhite icon--arrow"></span>
    </div>
    <div class="search-filtersWrap">
      <div class="search-filter toggle">
        <input type="checkbox" class="toggle-checkbox" id="mobile-filter-where">
        <label class="search-filterTitle toggle-label" for="mobile-filter-where">
          Where
          <span class="icon--arrowRightWhite icon--arrow"></span>
        </label>
        <div class="search-filterItems toggle-container">
          <div class="search-addFilterItem select">
            <select>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
            </select>
          </div>
          <div class="search-filterItem">
            Maldiverne
            <span class="search-filterItemRemove"></span>
          </div>
          <div class="search-filterItem">
            Mauritius
            <span class="search-filterItemRemove"></span>
          </div>
          <div class="search-filterItem">
            Mozambique
            <span class="search-filterItemRemove"></span>
          </div>
          <div class="search-filterItem">
            Seychellerne
            <span class="search-filterItemRemove"></span>
          </div>
          <div class="search-filterItem">
            Sri Lanka
            <span class="search-filterItemRemove"></span>
          </div>
          <div class="search-filterItem">
            Zanzibar
            <span class="search-filterItemRemove"></span>
          </div>
        </div>
      </div> <!-- search filter where -->
      <div class="search-filter toggle">
        <input type="checkbox" class="toggle-checkbox" id="mobile-filter-what">
        <label class="search-filterTitle toggle-label" for="mobile-filter-what">
          What
          <span class="icon--arrowRightWhite icon--arrow"></span>
        </label>
        <div class="search-filterItems toggle-container">
          <div class="search-addFilterItem select">
            <select>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
            </select>
          </div>
          <div class="search-filterItem">
            Bryllupsrejser
            <span class="search-filterItemRemove"></span>
          </div>
          <div class="search-filterItem">
            All Inclusive rejser
            <span class="search-filterItemRemove"></span>
          </div>
          <div class="search-filterItem">
            Vip All Inclusive
            <span class="search-filterItemRemove"></span>
          </div>
          <div class="search-filterItem">
            Golfrejser
            <span class="search-filterItemRemove"></span>
          </div>
        </div>
      </div> <!-- search filter what -->
      <div class="search-filter toggle">
        <input type="checkbox" class="toggle-checkbox" id="mobile-filter-when">
        <label class="search-filterTitle toggle-label" for="mobile-filter-when">
          When
          <span class="icon--arrowRightWhite icon--arrow"></span>
        </label>
        <div class="search-filterItems toggle-container">
          <div class="search-addFilterItem select">
            <select>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
            </select>
          </div>
          <div class="search-filterItem">
            Bryllupsrejser
            <span class="search-filterItemRemove"></span>
          </div>
          <div class="search-filterItem">
            All Inclusive rejser
            <span class="search-filterItemRemove"></span>
          </div>
          <div class="search-filterItem">
            Vip All Inclusive
            <span class="search-filterItemRemove"></span>
          </div>
          <div class="search-filterItem">
            Golfrejser
            <span class="search-filterItemRemove"></span>
          </div>
        </div>
      </div> <!-- search filter when -->
      <div class="search-filter toggle">
        <input type="checkbox" class="toggle-checkbox" id="mobile-filter-price">
        <label class="search-filterTitle toggle-label" for="mobile-filter-price">
          Price
          <span class="icon--arrowRightWhite icon--arrow"></span>
        </label>
        <div class="search-filterItems search-filterItems--price toggle-container">
          <div class="js--search-priceSlider search-priceSlider">
            <div class="search-priceValues">
              <div class="search-price search-price--from">
                <span class="js--price-from">0.000</span>
                <span>DDK</span>
              </div>
              <div class="search-price search-price--to">
                <span class="js--price-to">0.000</span>
                <span>DDK</span>
              </div>
            </div>
            <div class="js--price-slider"></div>
          </div>
        </div>
      </div> <!-- search filter price-->
      <div class="search-filter toggle">
        <input type="checkbox" class="toggle-checkbox" id="mobile-filter-rating">
        <label class="search-filterTitle toggle-label" for="mobile-filter-rating">
          <form class="rate rate--white">
            <input type="radio" name="rate" class="rate-starCheckbox" id="rate-star--1">
            <label for="rate-star--1" class="rate-star"></label>
            <input type="radio" name="rate" class="rate-starCheckbox" id="rate-star--2">
            <label for="rate-star--2" class="rate-star"></label>
            <input type="radio" name="rate" class="rate-starCheckbox" id="rate-star--3">
            <label for="rate-star--3" class="rate-star"></label>
            <input type="radio" name="rate" class="rate-starCheckbox" id="rate-star--4" checked="checked">
            <label for="rate-star--4" class="rate-star"></label>
            <input type="radio" name="rate" class="rate-starCheckbox" id="rate-star--5">
            <label for="rate-star--5" class="rate-star"></label>
          </form>
          <span class="icon--arrowRightWhite icon--arrow"></span>
        </label>
        <div class="search-filterItems search-filterItems--rating toggle-container">
          <form class="rate rate--gold rate--active">
            <input type="radio" name="rate" class="rate-starCheckbox" id="rate-star--1">
            <label for="rate-star--1" class="rate-star"></label>
            <input type="radio" name="rate" class="rate-starCheckbox" id="rate-star--2">
            <label for="rate-star--2" class="rate-star"></label>
            <input type="radio" name="rate" class="rate-starCheckbox" id="rate-star--3">
            <label for="rate-star--3" class="rate-star"></label>
            <input type="radio" name="rate" class="rate-starCheckbox" id="rate-star--4" checked="checked">
            <label for="rate-star--4" class="rate-star"></label>
            <input type="radio" name="rate" class="rate-starCheckbox" id="rate-star--5">
            <label for="rate-star--5" class="rate-star"></label>
          </form>
        </div>
      </div> <!-- search filter rating-->
    </div>
    <div class="search-filterResults">
      <button class="button button--main button--block button--large">Show results (1449)</button>
    </div>
  </div>

  <div class="search-results text--center bg-darken--2 px--3 py--4 py--md--5">
    Hoteller pa Maldiverne: <strong>35</strong> resultater
  </div>
  <div class="container--lg pb--5 py--md--8">
    <div class="grid">
      <div class="col--12 col--md--4 mb--4 mb--md--0 hidden--sm--down">
        <form class="search-quick p--3 p--md--5 mb--5">
          <div class="select select--white w--100">
            <select name="where" id="">
              <option value="" disabled selected>Where?</option>
              <option value="">Option 1</option>
              <option value="">Option 2</option>
              <option value="">Option 3</option>
              <option value="">Option 4</option>
            </select>
          </div>
          <div class="select select--white w--100">
            <select name="what" id="">
              <option value="" disabled selected>What?</option>
              <option value="">Option 1</option>
              <option value="">Option 2</option>
              <option value="">Option 3</option>
              <option value="">Option 4</option>
            </select>
          </div>
          <div class="select-group d--flex">
            <div class="select select--white select--calendar w--50">
              <select name="where" id="">
                <option value="" disabled selected>Depart</option>
                <option value="">Option 1</option>
                <option value="">Option 2</option>
                <option value="">Option 3</option>
                <option value="">Option 4</option>
              </select>
            </div>
            <div class="select select--white select--calendar w--50">
              <select name="what" id="">
                <option value="" disabled selected>Return</option>
                <option value="">Option 1</option>
                <option value="">Option 2</option>
                <option value="">Option 3</option>
                <option value="">Option 4</option>
              </select>
            </div>
          </div> <!-- select group -->

          <div class="select select--white w--100">
            <select name="what" id="">
              <option value="" disabled selected>Number of persons?</option>
              <option value="">Option 1</option>
              <option value="">Option 2</option>
              <option value="">Option 3</option>
              <option value="">Option 4</option>
            </select>
          </div>
          <button class="button button--main button--block button--large">Søg</button>
        </form>
        <div class="search-filter bg-lighten--1">
          <header class="search-filterHeader bg-darken--4 py--4 px--5">
            <span class="icon--sliders mr--3"></span>
            <span class="">Filter results</span>
            <a href="" class="search-clear">Clear all</a>
          </header>
          <div class="search-filterBody">
            <div class="search-section search-section--query">
              <div class="search-query d--flex">
                <input class="input input--text w--100" type="text" placeholder="Hotelname">
                <button class="button button--main">Apply</button>
              </div>
            </div>
            <div class="search-section">
              <header class="search-sectionHeader">
                <span>Where</span>
                <a href="" class="search-clear">Clear</a>
              </header>
              <div class="search-sectionBody">
                <div class="checkbox">
                  <input type="checkbox" name="all-1" id="all--1">
                  <label class="text--uppercase" for="all--1">Det Indiske Ocean</label>
                </div>
                <div class="search-checkboxes-2columns">
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">Maldiverne</label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">Mauritius</label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">Mozanbique</label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">Seychellerne</label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">Sri lanka</label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">Zanzibar</label>
                  </div>
                </div> <!-- search-checkboxes-2columns -->

                <div class="spacer mb--5"></div>

                <div class="checkbox">
                  <input type="checkbox" name="all-1" id="all--1">
                  <label class="text--uppercase" for="all--1">Afrika</label>
                </div>
                <div class="search-checkboxes-2columns">
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">Botsuana</label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1" checked="checked">
                    <label class="" for="all--1">Kenya</label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">Namibia</label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">Rwanda</label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">Sydafrika</label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">Tanzania</label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">Uganda</label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">Zambia</label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">Zimbambwe</label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">Zanzibar</label>
                  </div>
                </div> <!-- search-checkboxes-2columns -->

                <div class="spacer mb--5"></div>

                <div class="checkbox">
                  <input type="checkbox" name="all-1" id="all--1">
                  <label class="text--uppercase" for="all--1">Mellemosten</label>
                </div>
                <div class="search-checkboxes-2columns">
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">Abu Dhabi</label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">Dubai</label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">Oman</label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">Ras of Khaimanh</label>
                  </div>
                </div> <!-- search-checkboxes-2columns -->

              </div> <!-- search-sectionBody -->
            </div> <!-- search-section -->
            <div class="search-section">
              <header class="search-sectionHeader">
                <span>What</span>
                <a href="" class="search-clear">Clear</a>
              </header>
              <div class="search-sectionBody">
                <div class="checkbox">
                  <input type="checkbox" name="all-1" id="all--1">
                  <label class="text--uppercase" for="all--1">Alle Rejsertype</label>
                </div>
                <div class="search-checkboxes-2columns">
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">Brullupsrejser</label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">All Inclusive</label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">VIP All Inclusive</label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">Golfrejser</label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">Eksotiska vejser</label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">Safarirejser til Afrika</label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">Rejser med born</label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">Adventure rejser</label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">Togrejser</label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">Kombinationsrejser</label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">Luksusrejser</label>
                  </div>
                </div> <!-- search-checkboxes-2columns -->

              </div> <!-- search-sectionBody -->
            </div> <!-- search-section -->
            <div class="search-section">
              <header class="search-sectionHeader">
                <span>Kriterier</span>
                <a href="" class="search-clear">Clear</a>
              </header>
              <div class="search-sectionBody">
                <div class="checkbox">
                  <input type="checkbox" name="all-1" id="all--1">
                  <label class="" for="all--1">Privat pool</label>
                </div>
                <div class="checkbox">
                  <input type="checkbox" name="all-1" id="all--1">
                  <label class="" for="all--1">Privat villa</label>
                </div>
                <div class="checkbox">
                  <input type="checkbox" name="all-1" id="all--1">
                  <label class="" for="all--1">Velegnet for pat</label>
                </div>
                <div class="checkbox">
                  <input type="checkbox" name="all-1" id="all--1">
                  <label class="" for="all--1">All inclusive</label>
                </div>
                <div class="checkbox">
                  <input type="checkbox" name="all-1" id="all--1">
                  <label class="" for="all--1">Egnet for famllier</label>
                </div>
              </div> <!-- search-sectionBody -->
            </div> <!-- search-section -->
            <div class="search-section">
              <header class="search-sectionHeader">
                <span>Price</span>
              </header>
              <div class="search-sectionBody">
                <div class="js--search-priceSlider search-priceSlider mb--7">
                  <div class="search-priceValues">
                    <div class="search-price search-price--from">
                      <span class="js--price-from">0.000</span>
                      <span>DDK</span>
                    </div>
                    <div class="search-price search-price--to">
                      <span class="js--price-to">0.000</span>
                      <span>DDK</span>
                    </div>
                  </div>
                  <div class="js--price-slider"></div>
                </div>
                <button class="button button--main button--block button--large">Show results (1449)</button>
              </div>
            </div>
          </div>
        </div>
      </div> <!-- search-wrap -->
      <div class="col--12 col--md--8">
        <div class="search-order mb--3 mb--md--5">
          <div class="select">
            <select name="" id="">
              <option value="">Our fauvourites</option>
            </select>
          </div>
          <div class="select">
            <select name="" id="">
              <option value="">Star - High</option>
            </select>
          </div>
          <div class="select">
            <select name="" id="">
              <option value="">Price -High</option>
            </select>
          </div>

        </div>
        <div class="grid tour-list tour-list--search">
          <?php for($i=0;$i<15;$i++):?>
            <div class="col--12 col--md--6">
              <?php include('partials/tour-list-item.php'); ?>
            </div>
          <?php endfor; ?>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="section--blockquote blockquote-widget py--7" id="section--8" style="background: url(assets/images/blockquote-example.jpg) center/cover no-repeat">
  <blockquote class="blockquote-widgetContainer container--xs py--5 px--4 py--md--7 px--md--8 text--center" style="background: white">
    <cite class="blockquote-cite mb--4">
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum cum architecto, molestias fuga excepturi, nostrum nesciunt quam, pariatur, praesentium amet voluptas velit alias libero perspiciatis! Eaque aperiam nam aut voluptate?
    </cite>
    <div class="blockquote-author">Lovely planet</div>
  </blockquote>
</section> <!-- section 8-->
<section class="section--wide" id="section--9">
  <!-- Slider big -->
  <div class="slider slider--medium mh--md--75vh">
    <div class="slider-inner js--slider slider--dots">
      <div class="slider-item" style="background-image:url('assets/images/slider/anantara-kihavah-first-class-holiday.jpg')"></div>
      <div class="slider-item" style="background-image:url('assets/images/slider/anantara-kihavah-maldives-island-resort.jpg')"></div>
      <div class="slider-item" style="background-image:url('assets/images/slider/anantara-kihavah-maldives-overwater-spa.jpg')"></div>
      <div class="slider-item" style="background-image:url('assets/images/slider/anantara-kihavah-maldives-villa-walkway.jpg')"></div>
    </div>
    <div class="slider-container px--4 px--md--0">
      <div class="container--xl pb--4 pt--8 py--md--8 justifyContent--end">
        <h3>Anantara
          <br>
          Kihavah Villas
        </h3>
        <div class="mb--0 mb--md--6">
          <img src="assets/images/1x/icon--couple.png"/>
          <img src="assets/images/1x/icon--family.png"/>
          <img src="assets/images/1x/icon--island.png"/>
          <img src="assets/images/1x/icon--scubadiving.png"/>
        </div>
        <div class="js--slider-actions slider-actions"></div>
      </div>
    </div>
  </div>
  <!-- Slider big end. -->
</section> <!-- section 9 -->