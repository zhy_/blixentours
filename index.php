<?php include_once('partials/header.php'); ?>

  <main class="main main--global">
    <div class="main-inner">

      <?php if(!isset($_GET['page'])): ?>
        <?php include_once('pages/page--1.php'); ?>
      <?php else: ?>
        <?php include_once('pages/page--2.php'); ?>
      <?php endif; ?>

    </div>
  </main>

<?php include_once('partials/footer.php'); ?>