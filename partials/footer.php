
    <footer class="footer footer--global">
      <div class="footer-inner">
        <div class="footer-columns mb--sm--6">
          <div class="footer-column footer-column--left">
            <h5 class="">
              BLIXEN TOURS
              <br>
              <span class="font--italic">— Vi skaber øjeblikke</span>
            </h5>
            <p class="text--leader mb--5 hidden--sm--down">Store rejseoplevelser handler ofte om små detaljer. Dem som gør forstkellen og gør en god ferie til en uforglemmelig rejse. Det er detaljerne, der forvandler glæde til lykke og hyggelige stunder til uforglemmelige øjeblikke. Det er dét vi brænder for – at skabe disse øjeblikke.</p>
            <p class="hidden--sm--down">
              <strong class="text--uppercase">Vi skræddersyr rejser</strong>
              <br>
              Vi er specialister, fordi vi har været der – mærket øjeblikke der satte sig i sjælen. Vi kender hver eneste af vores destinationer i Afrika, Det Indiske Ocean og Mellemøsten. Vi formidler øjeblikke og deler dem – med erfaring, viden og passion. Vi skræddersyr rejser efter vores kunders ønsker og drømme – med kvalitet i hver en detalje.
            </p>
            <span class="logo logo--footer hidden--sm--down"></span>

          </div> <!-- footer-column--left -->

          <div class="footer-column footer-column--right">
            <div class="facebook--footer">
              <div class="facebook-title text--meta">Facebook</div>
              <p class="facebook-text text--leader">
                Få eksklusive rejsetilbud 
                <br>
                og læs spændende rejsefortællinger
              </p>
              <a href="#" class="button button--facebookFooter"><span class="fa fa-fw fa-facebook"></span> følg os på facebook</a>
            </div>
            <nav class="nav--footer d--flex hidden--sm--down">
              <div class="nav-list nav-list--first mr--6">
                <h6 class="font--italic text--uppercase mb--0">Destinationer</h6>
                <div class="nav-item"><a href="" class="nav-link">Maldiverne</a></div>
                <div class="nav-item"><a href="" class="nav-link">Mauritius</a></div>
                <div class="nav-item"><a href="" class="nav-link">Seychellerne</a></div>
                <div class="nav-item"><a href="" class="nav-link">Zanzibar</a></div>
                <div class="nav-item"><a href="" class="nav-link">Dubai</a></div>
                <div class="nav-item"><a href="" class="nav-link">Botswana </a></div>
                <div class="nav-item"><a href="" class="nav-link">Kenya </a></div>
                <div class="nav-item"><a href="" class="nav-link">Sydafrika </a></div>
                <div class="nav-item"><a href="" class="nav-link">Tanzania</a></div>
              </div>
              <div class="nav-list nav-list--last">
                <h6 class="font--italic text--uppercase mb--0">Rejsetyper</h6>
                <div class="nav-item"><a href="" class="nav-link">VIP All Inclusive</a></div>
                <div class="nav-item"><a href="" class="nav-link">Bryllupsrejser</a></div>
                <div class="nav-item"><a href="" class="nav-link">Golfrejser</a></div>
                <div class="nav-item"><a href="" class="nav-link">Eksotiske rejser</a></div>
                <div class="nav-item"><a href="" class="nav-link">Rejser med børn</a></div>
                <div class="nav-item"><a href="" class="nav-link">Adventurerejser</a></div>
                <div class="nav-item"><a href="" class="nav-link">Togrejser</a></div>
                <div class="nav-item"><a href="" class="nav-link">Kombinationsrejser</a></div>
                <div class="nav-item"><a href="" class="nav-link">Luksusrejser</a></div>
                <div class="nav-item"><a href="" class="nav-link">Safarirejser</a></div>
              </div>
            </nav>
          </div> <!-- footer-column--right -->
        </div> <!-- footer-columns -->
        <div class="footer-bottom">
          <div class="footer-address text--sm--justify mr--sm--3 w--100 d--flex flex--column flex--sm--row flex--sm--nowrap justifyContent--sm--between pr--0 pr--lg--6 mb--5 mb--lg--0">
            <span>Blixen Tours A/S</span>
            <span>Helge Nielsens Allé 7, 1C</span>
            <span>DK - 8723 Løsning</span>
            <span>CVR. 27129218</span>
            <span<+45 7674 0070</span>
            <span>post@blixentours.dk</span>
          </div>
          <div class="social social--footer text--center text--lg--left">
            <a href="#"><span class="fa fa-facebook"></span></a>
            <a href="#"><span class="fa fa-twitter"></span></a>
            <a href="#"><span class="fa fa-google-plus"></span></a>
            <a href="#"><span class="fa fa-vimeo"></span></a>
          </div>
          <span class="logo logo--footer hidden--sm--up"></span>
        </div>
      </div>
    </footer>
  </div> <!-- .page-wrap -->

  <script src="assets/js/vendor/jquery.min.js"></script>
  <script src="assets/js/vendor/jquery-ui.min.js"></script>
  <script src="assets/js/vendor/modernizr.min.js"></script>
  <script src="assets/js/map.js"></script>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD0rMNQlte4JGaQ2yhXF8dpFBXwSNmf32Y&callback=initMap"></script>
  <script src="assets/js/vendor/slick.min.js"></script>
  <script src="assets/js/vendor/visible.js"></script>
  <script src="assets/js/vendor/lightbox.min.js"></script>
  <script src="assets/js/app.js?v=2"></script>

</body>
</html>