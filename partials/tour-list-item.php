<div class="tour tour--vip tour--listItem">
  <div class="tour-inner bg-variant-1--3">
    <img class="tour-image image--contain" src='assets/images/tour-item.jpg'>
    <div class="tour-info">
      <div class="tour-badge tour-badge--gold"></div>
      <h4 class="tour-title">i skyggen af kilimanjaro og det indiske ocean</h4>
      <p class="tour-location">Kenya & Dubai</p>
      <div class="tour-rate">
        <form class="rate">
          <input type="radio" name="rate" class="rate-starCheckbox" id="rate-star--1" checked="checked">
          <label for="rate-star--1" class="rate-star"></label>
          <input type="radio" name="rate" class="rate-starCheckbox" id="rate-star--2">
          <label for="rate-star--2" class="rate-star"></label>
          <input type="radio" name="rate" class="rate-starCheckbox" id="rate-star--3">
          <label for="rate-star--3" class="rate-star"></label>
          <input type="radio" name="rate" class="rate-starCheckbox" id="rate-star--4">
          <label for="rate-star--4" class="rate-star"></label>
          <input type="radio" name="rate" class="rate-starCheckbox" id="rate-star--5">
          <label for="rate-star--5" class="rate-star"></label>
        </form>
      </div>
      <div class="tour-details">
        <div class="tour-vip"></div>
        <div class="tour-meta">
          <small>fra</small>
          <div class="tour-price">kr. 39.095</div>
          <small class="tour-duration">12 dage / 8 nætter</small>
        </div>
      </div>
    </div>
  </div>
</div>