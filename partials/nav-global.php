<nav class="nav nav--global">
  <div class="nav-list">
    <div class="nav-item nav-item--hasSubnav">
      <a href="#" class="nav-link">Destinationer</a>
      <div class="nav-subnav">
        <div class="nav-subnavInner nav-columns">
          
          <div class="nav-column nav-column--1of3">
            <a href="#" class="button button--variant--1 button--block button--navDestination js--button-navDestination">Det Indiske Ocean</a>
            <br>
            <div class="nav nav--list mb--md--5">
              <div class="nav-list">
                <div class="nav-item">
                  <a href="#" class="nav-link">Maldiverne</a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">Mauritius</a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">Mozambique</a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">Seychellerne</a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">Sri Lanka</a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">Zanzibar</a>
                </div>
              </div>
            </div>

            <div class="media-item hidden--sm--down">
              <div class="media-image">
                <img class="image--contain" src="http://via.placeholder.com/350x150" alt="">
              </div>
              <div class="media-body">
                <header class="media-header">
                  <h4 class="media-title">One&Only Royal Mirage</h4>
                  <p class="media-subtitle">/ Dubai</p>
                </header>
                <p class="media-text">Blixen Tours er et danskejet rejsebureau, der specialiserer sig i at arrangere rejser til Afrika, Det Indiske Ocean og Mellemøsten. MORE</p>
              </div> <!-- media-body -->
            </div> <!-- media-item -->

          </div> <!-- nav-column -->
          
          <div class="nav-column nav-column--1of3">
            <a href="#" class="button button--variant--2 button--block button--navDestination js--button-navDestination">Afrika</a>
            <br>
            <div class="nav nav--list mb--md--5">
              <div class="nav-list">
                <div class="nav-item">
                  <a href="#" class="nav-link">Botswana</a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">Kenya</a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">Rwanda</a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">Namibia</a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">Sydafrika</a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">Tanzania</a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">Uganda  </a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">Zimbabwe</a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">Zambia</a>
                </div>
              </div>
            </div>

            <div class="media-item media-item--small hidden--sm--down">
              <div class="media-image float--left mr--3">
                <img class="image--contain" src="http://via.placeholder.com/80x120" alt="">
              </div>
              <div class="media-body">
                <header class="media-header">
                  <h4 class="media-title">The Big Five</h4>
                  <p class="media-subtitle">/ Kenya</p>
                </header>
                <p class="media-text">Blixen Tours er et danskejet rejsebureau, der specialiserer sig i at arrangere  MORE</p>
              </div> <!-- media-body -->
            </div> <!-- media-item -->

          </div> <!-- nav-column -->
          
          <div class="nav-column nav-column--1of3">
            <a href="#" class="button button--variant--3 button--block button--navDestination js--button-navDestination">Mellemøsten</a>
            <br>
            <div class="nav nav--list mb--md--5">
              <div class="nav-list">
                <div class="nav-item">
                  <a href="#" class="nav-link">Oman</a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">Abu Dhabi</a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">Dubai</a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">Ras Al Khaimah</a>
                </div>
              </div>
            </div>

            <div class="media-item hidden--sm--down">
              <div class="media-image">
                <img class="image--contain" src="http://via.placeholder.com/350x350" alt="">
              </div>
              <div class="media-body">
                <header class="media-header">
                  <h4 class="media-title">One&Only Royal Mirage</h4>
                  <p class="media-subtitle">/ Dubai</p>
                </header>
                <p class="media-text">Blixen Tours er et danskejet rejsebureau, der specialiserer sig i at arrangere rejser til Afrika, Det Indiske Ocean og Mellemøsten. MORE</p>
              </div> <!-- media-body -->
            </div> <!-- media-item -->

          </div> <!-- nav-column -->
        </div> <!-- nav-subnavInner -->
      </div> <!-- nav-subnav -->
    </div> <!-- nav-item -->
    <div class="nav-item nav-item--hasSubnav">
      <a href="#" class="nav-link">Rejsetyper</a>

      <div class="nav-subnav nav-subnav--columns">
        <div class="nav-subnavInner  nav-columns">
          
          <div class="nav-column nav-column--1of3">
            <h5 class="nav-title">Rejsetyper</h5>
            <div class="nav nav--list mb--5">
              <div class="nav-list">
                <div class="nav-item">
                  <a href="#" class="nav-link">Blixen Tours VIP All Inclusive</a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">Bryllupsrejser</a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">All Inclusive</a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">Golfrejser</a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">Eksotiske rejser</a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">Rejser med børn</a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">Adventurerejser</a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">Togrejser</a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">Kombinationsrejser</a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">Luksusrejser</a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">Safarirejser</a>
                </div>
              </div>
            </div>


          </div> <!-- nav-column -->
          
          <div class="nav-column nav-column--2of3">
            <div class="d--md--flex mb--md--6 ">

              <div class="media-item mb--6 mb--md--0">
                <div class="media-image">
                  <img class="image--contain" src="http://via.placeholder.com/450x270" alt="">
                </div>
                <div class="media-body">
                  <header class="media-header">
                    <h4 class="media-title">One&Only Royal Mirage</h4>
                    <p class="media-subtitle">/ Dubai</p>
                  </header>
                </div> <!-- media-body -->
              </div> <!-- media-item -->


              <div class="media-item ml--md--6 mb--6 mb--md--0">
                <div class="media-image">
                  <img class="image--contain" src="http://via.placeholder.com/200x200" alt="">
                </div>
                <div class="media-body">
                  <header class="media-header">
                    <h4 class="media-title">Safarirejser</h4>
                    <p class="media-subtitle">/ Afrika</p>
                  </header>
                </div> <!-- media-body -->
              </div> <!-- media-item -->

            </div> <!-- d--flex -->

            <div class="media-item">
              <div class="media-image">
                <img class="image--contain" src="http://via.placeholder.com/700x300" alt="">
              </div>
              <div class="media-body">
                <header class="media-header">
                  <h4 class="media-title">Blixen Tours</h4>
                  <p class="media-subtitle">/ Vip All inclusive</p>
                </header>
              </div> <!-- media-body -->
            </div> <!-- media-item -->

          </div> <!-- nav-column -->
          
        </div> <!-- nav-subnavInner -->
      </div> <!-- nav-subnav -->
    </div> <!-- nav-item -->
    <div class="nav-item nav-item--hasSubnav">
      <a href="#" class="nav-link">Inspirations</a>
      <div class="nav-subnav">
        <div class="nav-subnavInner">
          <div class="d--md--flex mb--md--4">

            <div> <!-- flex wrap -->

              <div class="d--md--flex mb--md--6">

                <div class="media-item mb--6 mb--md--0">
                  <div class="media-image">
                    <img class="image--contain" src="http://via.placeholder.com/450x270" alt="">
                  </div>
                  <div class="media-body">
                    <header class="media-header">
                      <h4 class="media-title">One&Only Royal Mirage</h4>
                      <p class="media-subtitle">/ Dubai</p>
                    </header>
                  </div> <!-- media-body -->
                </div> <!-- media-item -->


                <div class="media-item ml--md--6 mb--6 mb--md--0">
                  <div class="media-image">
                    <img class="image--contain" src="http://via.placeholder.com/200x200" alt="">
                  </div>
                  <div class="media-body">
                    <header class="media-header">
                      <h4 class="media-title">Safarirejser</h4>
                      <p class="media-subtitle">/ Afrika</p>
                    </header>
                  </div> <!-- media-body -->
                </div> <!-- media-item -->
              </div> <!-- d--flex -->

              <div class="media-item mb--6 mb--md--0">
                <div class="media-image">
                  <img class="image--contain" src="http://via.placeholder.com/700x300" alt="">
                </div>
                <div class="media-body">
                  <header class="media-header">
                    <h4 class="media-title">Blixen Tours</h4>
                    <p class="media-subtitle">/ Vip All inclusive</p>
                  </header>
                </div> <!-- media-body -->
              </div> <!-- media-item -->

            </div>  <!-- flex wrap -->

            <div class="ml--md--6">  <!-- flex wrap -->
              <div class="media-item mb--6">
                <div class="media-image">
                  <img class="image--contain" src="http://via.placeholder.com/700x300" alt="">
                </div>
                <div class="media-body">
                  <header class="media-header">
                    <h4 class="media-title">Blixen Tours</h4>
                    <p class="media-subtitle">/ Vip All inclusive</p>
                  </header>
                </div> <!-- media-body -->
              </div> <!-- media-item -->

              <div class="media-item mb--6 mb--md--0">
                <div class="media-image">
                  <img class="image--contain" src="http://via.placeholder.com/700x300" alt="">
                </div>
                <div class="media-body">
                  <header class="media-header">
                    <h4 class="media-title">Blixen Tours</h4>
                    <p class="media-subtitle">/ Vip All inclusive</p>
                  </header>
                </div> <!-- media-body -->
              </div> <!-- media-item -->
            </div>  <!-- flex wrap -->
          </div> <!-- d--flex -->

          <div class="text--center">
            <a href="#" class="button button--main button--big">Se alle</a>
          </div>

        </div> <!-- nav-subnavInner -->
      </div> <!-- nav-subnav -->
    </div> <!-- nav-item -->
    <div class="nav-item nav-item--hasSubnav">
      <a href="#" class="nav-link">Katalog</a>
      <div class="nav-subnav">
        <div class="nav-subnavInner">
          <div class="d--md--flex mb--7 catalog catalog--nav">

            <img class="image--contain mb--3 mb--md--0" src="http://via.placeholder.com/500x500" alt="">

            <div class="ml--md--6">  <!-- flex wrap -->
            
              <p class="catalog-subtitle">Lorem ipsum dolor sit amet.</p>
              <h3 class="catalog-title">Lorem ipsum dolor sit.</h3>
              <img class="image--contain" src="assets/images/blixen-tours-platinum.png" alt="">

            </div>  <!-- flex wrap -->
          </div> <!-- d--flex -->

          <div class="text--center">
            <a href="#" class="button button--main button--big">Se alle</a>
          </div>
        </div> <!-- nav-subnavInner -->
      </div> <!-- nav-subnav -->
    </div> <!-- nav-item -->
    <div class="nav-item nav-item--hasSubnav">
      <a href="#" class="nav-link">Om Blixen Tours</a>
      <div class="nav-subnav nav-subnav--columns">
        <div class="nav-subnavInner nav-columns">
          
          <div class="nav-column nav-column--1of3 mb--3 mb--md--0">
            <h5 class="nav-title">Om Blixen Tours</h5>
            <div class="nav nav--list mb--5">
              <div class="nav-list">
                <div class="nav-item">
                  <a href="#" class="nav-link">Om Blixen Tours</a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">Ledige stillinger</a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">Generelle rejsebetingelser</a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">Betaling</a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">Presse</a>
                </div>
                <div class="nav-item">
                  <a href="#" class="nav-link">Kontakt os</a>
                </div>
              </div>
            </div>
            <a href="#" class="button button--block button--dark">Kontakt os</a>

          </div> <!-- nav-column -->
          
          <div class="nav-column nav-column--2of3">
            
            <div class="media-item ml--md--6">
              <div class="media-image">
                <img class="image--contain" src="http://via.placeholder.com/450x450" alt="">
              </div>
              <div class="media-body">
                <header class="media-header">
                  <h4 class="media-title">Safarirejser</h4>
                  <p class="media-subtitle">/ Afrika</p>
                </header>
              </div> <!-- media-body -->
            </div> <!-- media-item -->

          </div> <!-- nav-column -->
          
        </div> <!-- nav-subnavInner -->
      </div> <!-- nav-subnav -->
    </div> <!-- nav-item -->
  </div> <!-- nav-list -->
  <a href="#" class="js--nav-subnavClose nav-subnavClose"></a>
  <div class="nav-aside">
    <div class="nav-asideInner">
      <div class="nav-asideWrap">
        <a href="" class="button button--navContactUs button--main hidden--md--up"><span class="icon--phone--"></span></a>
        <div class="languages mr--md--3">
          <a href="" class="languages-item button button--small button--main-clear">NO</a>
          <a href="" class="languages-item button button--small button--main-clear button--active">DK</a>
        </div>
        <a href="#" class="favorite hidden--sm--down"><i class="fa fa-fw fa-heart-o"></i></a>
      </div>
    </div>
  </div>
</nav> <!-- nav -->