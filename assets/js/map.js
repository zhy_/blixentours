function initMap() {
  var mapOptions = {
      center: new google.maps.LatLng(54.721121, 25.231187),
      zoom: 15,
      zoomControl: true,
      zoomControlOptions: {
          style: google.maps.ZoomControlStyle.DEFAULT,
      },
      disableDoubleClickZoom: true,
      mapTypeControl: true,
      mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
      },
      scaleControl: true,
      scrollwheel: false,
      panControl: true,
      streetViewControl: true,
      draggable : true,
      overviewMapControl: true,
      overviewMapControlOptions: {
          opened: false,
      },
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}]
  }
  var map = new google.maps.Map(document.getElementById('map'), mapOptions);

  var marker, i;

  var locations = [
    ['1'],
    ['2'],
    ['3'],
    ['4'],
    ['5'],
    ['6']

  ];

  var posMarkers = {};  
  for (i = 0; i < locations.length; i++) {  
    // posMarkers[i] = new google.maps.Marker({
    //   position: new google.maps.LatLng(locations[i][3], locations[i][4]),
    //   icon: locations[i][2],
    //   map: map,
    //   zIndex: locations[i][6],
    //   visible: locations[i][5]
    // });

    // google.maps.event.addListener(posMarkers[i], 'click', (function(marker, i) {
    //   if(i==0) {
    //     return function() {
    //       window.open( "https://www.google.com/maps/search/?api=1&query="+locations[i][3]+","+locations[i][4], "_blank" );
    //      }
    //   }
    //   else {
    //     return function() {
    //       infowindow.setContent(locations[i][0]);
    //       infowindow.open(map, posMarkers[i]);
    //     }
    //   }
    // })(posMarkers[i], i));
  }

}
