
// Init jQuery
jQuery(document).ready(function($) {

  // Add class when el scrolls into viewport
  var visible = function() {
    $('.js--animate').each(function() {
      if($(this).visible(true) && !$(this).hasClass('animate')) {
        $(this).addClass('animate');
      }
    });
  }();

  // Add class when el scrolls into viewport
  var visibleFully = function() {
    $('.js--animate--full').each(function() {
      if($(this).visible() && !$(this).hasClass('animate')) {
        $(this).addClass('animate');
      }
    });
  }();

  var closeActiveModes = function() {
    $('body').removeClass('navDestinationActive nav-mobile--opened subnav--opened header-search--active header-search--hasResults');
    $('.nav-column').removeClass('nav-column--active');
    $('.js--button-navDestination').removeClass('button--active');
  };

  // Show mobile nav
  $(document).on('click touchend','.js--nav-toggle',function(e){
    e.stopPropagation();
    e.preventDefault();

    $('body').toggleClass('nav-mobile--opened');
  });;

  // Show search/filter block
  $(document).on('click touchend','.js--search-filterTrigger',function(e){
    e.stopPropagation();
    e.preventDefault();

    $('body').toggleClass('search-wrap--opened');
  });;

  // Show mobile nav
  $(document).on('click touchend','.js--button-navDestination',function(e){
    e.stopPropagation();
    e.preventDefault();

    if($(window).width()<=768) {
      $('body').removeClass('navDestinationActive');
      $('.nav-column').removeClass('nav-column--active');

      if(!$(this).hasClass('button--active')) {
        $('.js--button-navDestination').removeClass('button--active');
        $('.js--button-navDestination').parents('.nav-column').removeClass('nav-column--active');
        $('body').addClass('navDestinationActive');
        $(this).addClass('button--active');
        $(this).parents('.nav-column').addClass('nav-column--active');
      }
      else {
        $(this).removeClass('button--active');
      }
    }
  });

  // dirty hack
  // button uses box shadow for background
  // and has to have negative z-index which makes it unclickable.
  $(document).mouseup(function(e) {
    var container = $(".js--button-navDestination").parent('.nav-column');
    if (container.is(e.target) || container.has(e.target).length !== 0 && container.children('.nav').has(e.target).length === 0)  {
      $('.js--button-navDestination.button--active').trigger('click');
    }
  });

  // Show subnav
  $(document).on('click touchend','.nav--global .nav-item--hasSubnav > .nav-link ',function(e){
    e.stopPropagation();
    e.preventDefault();

    if($(this).closest('.nav-item').hasClass('nav-item--active')) {
      $('.nav--global .nav-item').removeClass('nav-item--active');
      $('body').removeClass('subnav--opened');
      return;
    }

    $('.nav--global .nav-item').removeClass('nav-item--active');
    $(this).closest('.nav-item').toggleClass('nav-item--active');

    if(!$('body').hasClass('subnav--opened')) {
      $('body').addClass('subnav--opening');
      $(this).siblings('.nav-subnav').one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend",function(e) {
        $('body').addClass('subnav--opened');
        $('body').removeClass('subnav--opening');
      });
    }
  });

  // Hide subnav
  $(document).on('click touchend','.subnav--opened .nav-subnavClose ',function(e){
    e.stopPropagation();
    e.preventDefault();

    $('body').addClass('subnav--closing');
    $('.nav-item--hasSubnav.nav-item--active').children('.nav-subnav').one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend",function(e) {
      $('.nav-item--hasSubnav.nav-item--active').removeClass('nav-item--active');
      $('body').removeClass('subnav--opened subnav--closing');
    });
  });
  // Esc button to close subnav
  $(document).keyup(function(e){
    if(e.keyCode === 27) {
      // $(".nav-subnavClose").trigger('click');
      closeActiveModes();
    }
  });

  // Rate label click (native click glitches)
  $(document).on('click touchend','label.rate-star ',function(e){
    e.stopPropagation();
    e.preventDefault();
    $(this).siblings('#'+$(this).attr('for')).trigger('click');
  });
  // Search toggle
  $(document).on('click touchend','.js--search-submit ',function(e){
    e.stopPropagation();
    e.preventDefault();
    $('body').toggleClass('header-search--active');
    if($('body').hasClass('header-search--hasResults')) {
      $('body').removeClass('header-search--hasResults')
    }
  });
  $(document).on('focus','.js--search-input ',function(e){
    e.stopPropagation();
    e.preventDefault();
    $('body').addClass('header-search--hasResults');
  });
  $(document).mouseup(function(e) {
    var container = $(".header-aside .search");
    if (!container.is(e.target) && container.has(e.target).length === 0)  {
      $('body').removeClass('header-search--hasResults header-search--active');
    }
  });

  var $root = $('html, body');
  $('.nav--page').find('.nav-link').click(function() {

    var href = $.attr(this, 'href');
    $root.animate({
      scrollTop: $(href).offset().top
    }, 500, function () {
      window.location.hash = href;
    });
    return false;
  });

  // Sliders
  $('.js--slider').each(function(index, el) {
    var $arrows = $(this).parents('.slider').find('.js--slider-actions');
    var dots = $(this).hasClass('slider--dots') ? true : false;
    $(this).slick({
      appendArrows: $arrows,
      autoplay: true,
      autoplaySpeed: 5000,
      speed: 1000,
      fade: true,
      cssEase: 'linear',
      dots: dots,
      infinite: true
    });
  });

  // Carousels
  $('.js--carousel').each(function(index, el) {
    var $arrows = $(this).siblings('.js--carousel-actions');
    $(this).slick({
      arrows: false,
      autoplay: true,
      autoplaySpeed: 5000,
      speed: 500,
      cssEase: 'linear',
      dots: true,
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 4,
      responsive: [
        {
          breakpoint: 2200,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 3,
          }
        },
        {
          breakpoint: 1500,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
  });

  // Accordion
  $( ".accordion" ).accordion({
    header: "h3.accordion-header",
    collapsible: true,
    icons: { "header": "", "activeHeader": "" },
    heightStyle: "content",
    classes: {
      "ui-accordion-header": "",
      "ui-accordion-header-collapsed": "accordion-header--collapsed",
      "ui-accordion-content": "accordion-content"
    }
  });

  // Filter price range slider
    if($( ".js--price-slider" ).length != 0) {
      $( ".js--price-slider" ).slider({
        range: true,
        min: 0,
        max: 1000000,
        step: 100,
        values: [ 10000, 500000 ],
        slide: function( event, ui ) {
          $( ".js--price-from" ).text(ui.values[ 0 ]);
          $( ".js--price-to" ).text(ui.values[ 1 ]);
        }
      });
      $( ".js--price-from" ).text( $( ".js--price-slider" ).slider( "values", 0 ) );
      $( ".js--price-to" ).text( $( ".js--price-slider" ).slider( "values", 1 ) );
    }

  // Window scroll
  $(window).scroll(function() {
    // Watch for el's visibility
    visible;
    visibleFully;
  });
  // $(window).resize(function() {
  //   // Remove all active modes
  //   if($(window).width()>767) {
  //     closeActiveModes();
  //   }
  // });
});