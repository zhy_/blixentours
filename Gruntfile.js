/**
 * Grunt commands
 *
 *  default         - watches styles
 *  dev             - builds dev
 *  build           - builds production
 *
 *
 */


module.exports = function (grunt) {

  // Load grunt modules

  require('load-grunt-tasks')(grunt);

  // Vars

  var path      = 'assets';
  var resources = 'resources/assets';
  var libsass   = false;
  var stylesDevTask, stylesWatchTask, stylesBuildTask

  // Project configuration.

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    /*-------------------------------------/
    //  Style tasks
    //------------------------------------*/

    compass: {
      options: {
        require : ['modernizr-mixin', 'sass-globbing']
      },
      dev: {
        options: {
          httpPath       : path,
          cssDir         : path + "/precss",
          sassDir        : resources + "/sass",
          imagesDir      : path + "/images",
          javascriptsDir : path + "/js",
          fontsDir       : path + "/fonts",
          outputStyle    : "expanded",
          relativeAssets : true,
          noLineComments : true,
          boring         : false,
          sourcemap      : true,
          watch          : true
        }
      },
      build: {
        options: {
          httpPath       : path,
          cssDir         : path + "/precss",
          sassDir        : resources + "/sass",
          imagesDir      : path + "/images",
          javascriptsDir : path + "/js",
          fontsDir       : path + "/fonts",
          outputStyle    : "compressed",
          environment    : 'production',
          relativeAssets : true,
          noLineComments : true,
          boring         : true,
          sourcemap      : false
        }
      }
    },
    sass: {
      dev: {
        options: {
          sourceMap: true
        },
        files: [{
           expand: true,
           cwd: resources + '/sass',
           src: ['**/*.sass'],
           dest: path + '/precss',
           ext: '.css'
        }]
      },
      build: {
        options: {
          sourceMap: false
        },
        files: [{
           expand: true,
           cwd: 'scss/',
           src: ['**/*.scss'],
           dest: 'css/',
           ext: '.css'
        }]
      }
    },
    postcss: {
      options: {
        syntax: require('postcss-scss'),
        parser: require('postcss-scss')
      },
      dev: {
        options: {
          map: true,
          processors: [
            require('postcss-sorting'),
            require('postcss-assets')({
              loadPaths: [path + "/images"]
            }),
            require('postcss-image-set-polyfill'),
            require('autoprefixer')({
              browsers: ['> 0%']
            }),
            require('postcss-initial')({
              reset: 'inherited'
            }),
            require('postcss-font-magician')({

            })
          ]
        },
        files: [{
          expand: true,
          cwd: path + "/precss/",
          src: ["*.css", "pages/**/*.css"],
          dest: path + "/css"
        }]
      },
      build: {
        options: {
          map: false,
          processors: [
            require('postcss-strip-inline-comments'),
            require('postcss-sorting'),
            require('postcss-assets')({
              loadPaths: [path + "/images"]
            }),
            require('postcss-image-set-polyfill'),
            require('autoprefixer')({
              browsers: ['> 0%']
            }),
            require('postcss-initial')({
              reset: 'inherited'
            }),
            require('postcss-font-magician')({

            })
          ]
        },
        files: [{
          expand: true,
          cwd: path + "/precss",
          src: ["*.css", "pages/**/*.css"],
          dest: path + "/css"
        }]
      },
      lint: {
        options: {
          processors: [
            require("stylelint")({ /* your options */ }),
            require("postcss-reporter")({ clearMessages: true })
          ]
        },
        files: [{
          expand: true,
          cwd: path + "/precss/",
          src: ["*.css", "pages/**/*.css"],
          dest: path + "/css"
        }]
      }
    }, // postcss

    /*-------------------------------------/
    //  Clean
    //------------------------------------*/

    clean: {
      cssmaps: [path + "/css/*.css.map"],
      precss: [path + "/precss"]
    },

    /*-------------------------------------/
    //  Concurrent
    //------------------------------------*/

    concurrent: {
      watch: {
        tasks: ['watch', 'compass:dev'],
        // tasks: ['watch', 'sass:dev'],
        options: {
          logConcurrentOutput: true
        }
      }
    },

    /*-------------------------------------/
    //  Assets
    //------------------------------------*/

    imagemin: {
      dynamic: {
        files: [{
          expand: true,
          src: [path + "/images/**/*.{png,jpg,gif,jpeg}"]
        }]
      }
    },

    /*-------------------------------------/
    //  Watch
    //------------------------------------*/

    watch: {
      postcss: {
        files: [path + "/precss/**/*.css"],
        tasks: ['postcss:dev']
      },
      css: {
        files: [path + "/css/**/*.css"]
      },
      livereload: {
        files: [path + "/css/**/*.css"],
        options: {
          livereload: true
        }
      }
    },
    notify: {
      sass_compiled: {
        options: {
          title: 'Styles compiled',
          message: 'Grunt finished compiling sass files.',
        }
      }
    }
  });


  // Tasks
  //

  if(libsass) {
    stylesWatchTask = 'watch';
    stylesDevTask   = 'sass:dev';
    stylesBuildTask = 'sass:build';
  }
  else {
    stylesWatchTask = 'concurrent:watch';
    stylesDevTask   = 'compass:dev';
    stylesBuildTask = 'compass:build';

  }

  // Default task.
  grunt.registerTask('default', [
    stylesWatchTask,
    'notify:sass_compiled'
  ]);

  // Dev
  grunt.registerTask('dev', [
    'postcss:dev',
    stylesDevTask,
    'notify:sass_compiled'
  ]);

  // Build
  grunt.registerTask('build', [
    'imagemin',
    stylesBuildTask,
    'postcss:build',
    'clean:precss'
  ]);

};
